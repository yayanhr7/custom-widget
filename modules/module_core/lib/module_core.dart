library module_core;

export 'package:module_core/common/api/api_client.dart';
//COMMON
//api
export 'package:module_core/common/api/api_failure.dart';
//api errors
export 'package:module_core/common/api/errors/bad_network_error.dart';
export 'package:module_core/common/api/errors/bad_request_error.dart';
export 'package:module_core/common/api/errors/connection_timeout_error.dart';
export 'package:module_core/common/api/errors/duplicate_value_error.dart';
export 'package:module_core/common/api/errors/internal_server_error.dart';
export 'package:module_core/common/api/errors/not_found_error.dart';
export 'package:module_core/common/api/errors/unauthorized_error.dart';
//di
export 'package:module_core/common/di/auto_route_di.dart';
export 'package:module_core/common/di/dio_di.dart';
export 'package:module_core/common/di/shared_preference_di.dart';
//network
export 'package:module_core/common/network/network_client.dart';
export 'package:module_core/common/urls/api_path.dart';
//utils

//CONFIG
export 'package:module_core/config/injectable.dart';
export 'package:module_core/config/router/app_router.dart';
// EXPORT FILE THAT HAVE BEEN CREATED HERE (shorted in folders name)

//widget
export 'package:module_core/widget/button/app_button.dart';
export 'package:module_core/widget/card/app_card.dart';
export 'package:module_core/widget/card/app_list_tile.dart';
export 'package:module_core/widget/theme/app_colors.dart';
export 'package:module_core/widget/text/app_text.dart';
export 'package:module_core/widget/toast/app_toast.dart';
export 'package:module_core/widget/textfield/app_text_field.dart';
export 'package:module_core/widget/avatar/app_avatar.dart';
export 'package:module_core/widget/snackbar/app_snackbar.dart';
export 'package:module_core/widget/alertdialog/alert_dialog.dart';
export 'package:module_core/widget/enum/button_size.dart';
export 'package:module_core/widget/enum/button_type.dart';
export 'package:module_core/widget/enum/text_field_type.dart';
export 'package:module_core/widget/enum/icon_position.dart';
export 'package:module_core/widget/enum/button_shape.dart';
export 'package:module_core/widget/enum/text_type.dart';
export 'package:module_core/widget/enum/avatar_type.dart';
export 'package:module_core/widget/enum/toast_position.dart';
export 'package:module_core/widget/enum/alert_type.dart';

export 'package:module_core/widget/page/login_page.dart';

export 'package:module_core/env.dart';
