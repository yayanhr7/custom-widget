// ignore_for_file: unused_field

import 'package:module_core/common/api/errors/unauthorized_error.dart';
import 'package:module_core/config/injectable.dart';
import 'package:module_core/config/router/app_router.dart';
import 'package:module_dependencies/module_dependencies.dart';

class UnauthorizedInterceptor extends Interceptor {
  final _appRouter = getIt<AppRouter>();

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    if (err.response?.statusCode == 401 ||
        err.response?.statusCode == 403 ||
        err.response?.statusCode == 419) {
      //_appRouter.replaceAll([const LoginRoute()]);
      return super.onError(UnauthorizedError(err), handler);
    }
    super.onError(err, handler);
  }
}
