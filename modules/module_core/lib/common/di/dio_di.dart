import 'package:module_dependencies/module_dependencies.dart';

@module
abstract class DioDi {
  @lazySingleton
  Dio get dio => Dio();
}
