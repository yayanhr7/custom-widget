import 'package:module_dependencies/module_dependencies.dart';

@module
abstract class SharedPreferencesDi {
  @lazySingleton
  Future<SharedPreferences> get sharedPreferences async =>
      SharedPreferences.getInstance();
}
