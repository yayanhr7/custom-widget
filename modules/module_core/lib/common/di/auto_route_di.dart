import 'package:module_dependencies/module_dependencies.dart';

import 'package:module_core/config/router/app_router.dart';

@module
abstract class AutoRouteDi {
  @lazySingleton
  AppRouter get appRouter => AppRouter();
}
