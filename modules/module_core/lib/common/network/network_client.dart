import 'package:module_dependencies/module_dependencies.dart';

@lazySingleton
class NetworkClient extends NetworkInfoBase {
  final Connectivity connectivity;

  NetworkClient(this.connectivity);

  @override
  Future<bool> get isConnected async {
    final result = await connectivity.checkConnectivity();
    return result != ConnectivityResult.none;
  }
}

abstract class NetworkInfoBase {
  Future<bool> get isConnected;
}
