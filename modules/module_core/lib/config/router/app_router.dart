import 'package:module_dependencies/module_dependencies.dart';

part 'app_router.gr.dart';

// Open `auto_router` in pub dev for more complete documentation
@AutoRouterConfig(replaceInRouteName: 'Screen,Route')
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [];
}
