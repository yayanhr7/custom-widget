import 'package:module_core/config/injectable.config.dart';
import 'package:module_dependencies/module_dependencies.dart';

final getIt = GetIt.instance;

@InjectableInit(
  initializerName: 'init',
  preferRelativeImports: true,
  asExtension: false,
)
void configureDependencies(String env) => init(
      getIt,
      environment: env,
    );
