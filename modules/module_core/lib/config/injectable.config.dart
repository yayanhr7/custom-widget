// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:module_dependencies/module_dependencies.dart' as _i4;

import '../common/api/api_client.dart' as _i7;
import '../common/di/auto_route_di.dart' as _i9;
import '../common/di/dio_di.dart' as _i10;
import '../common/di/shared_preference_di.dart' as _i11;
import '../common/network/network_client.dart' as _i6;
import '../env.dart' as _i5;
import '../module_core.dart' as _i8;
import 'router/app_router.dart' as _i3;

const String _dev = 'dev';
const String _prod = 'prod';

// initializes the registration of main-scope dependencies inside of GetIt
_i1.GetIt init(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  final autoRouteDi = _$AutoRouteDi();
  final dioDi = _$DioDi();
  final sharedPreferencesDi = _$SharedPreferencesDi();
  gh.lazySingleton<_i3.AppRouter>(() => autoRouteDi.appRouter);
  gh.lazySingleton<_i4.Dio>(() => dioDi.dio);
  gh.factory<_i5.Env>(
    () => _i5.DevEnv(),
    registerFor: {_dev},
  );
  gh.factory<_i5.Env>(
    () => _i5.ProdEnv(),
    registerFor: {_prod},
  );
  gh.lazySingleton<_i6.NetworkClient>(
      () => _i6.NetworkClient(gh<_i4.Connectivity>()));
  gh.lazySingletonAsync<_i4.SharedPreferences>(
      () => sharedPreferencesDi.sharedPreferences);
  gh.lazySingleton<_i7.ApiClient>(() => _i7.ApiClient(
        gh<_i4.Dio>(),
        gh<_i8.Env>(),
      ));
  return getIt;
}

class _$AutoRouteDi extends _i9.AutoRouteDi {}

class _$DioDi extends _i10.DioDi {}

class _$SharedPreferencesDi extends _i11.SharedPreferencesDi {}
