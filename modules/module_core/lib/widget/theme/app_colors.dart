import 'package:flutter/material.dart';

class AppColors {
  static const Color primary = Color(0xFFFF8402);
  static const Color secondary = Color(0xFF2D9CDB);
  static const Color error = Color(0xFFD32F2F);
  static const Color background = Color(0xFFF5F5F5);
  static const Color surface = Color(0xFFFFFFFF);
  static const Color onPrimary = Color(0xFFFFFFFF);
  static const Color onSecondary = Color(0xFF000000);
  static const Color onError = Color(0xFFFFFFFF);
  static const Color onBackground = Color(0xFF000000);
  static const Color onSurface = Color(0xFF000000);
  static const Color success = Color(0xff10DC60);
  static const Color info = Color(0xff33B5E5);
  static const Color warning = Color(0xffFFBB33);
  static const Color light = Color(0xffE0E0E0);
  static const Color dark = Color(0xff222428);
  static const Color white = Color(0xffffffff);
  static const Color focus = Color(0xff434054);
  static const Color alt = Color(0xff794c8a);
  static const Color transparent = Colors.transparent;
  static const Color form = Color(0xFFF1F6FB);
}
