import 'package:flutter/material.dart';

import 'package:module_core/widget/theme/app_colors.dart';
import 'package:module_core/widget/theme/app_text_style.dart';

final ThemeData appTheme = _appTheme();

ThemeData _appTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    colorScheme: base.colorScheme.copyWith(
      primary: AppColors.primary,
      onPrimary: Colors.white,
      secondary: const Color.fromARGB(255, 223, 27, 12),
      onSecondary: Colors.white,
      error: AppColors.error,
      background: const Color.fromARGB(255, 228, 243, 228),
      onBackground: Colors.black,
    ),
    textTheme: _appTextTheme(base.textTheme),
    appBarTheme: _appBarTheme(base.appBarTheme),
  );
}

TextTheme _appTextTheme(TextTheme base) => base.copyWith(
      displayLarge: base.displayLarge?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 14,
      ),
      displayMedium: base.displayMedium?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 12,
      ),
      displaySmall: base.displaySmall?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 11,
      ),
      headlineLarge: base.headlineLarge?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 32,
      ),
      headlineMedium: base.headlineMedium?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 28,
      ),
      headlineSmall: base.headlineSmall?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 24,
      ),
      titleLarge: base.titleLarge?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 22,
      ),
      titleMedium: base.titleMedium?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 16,
      ),
      titleSmall: base.titleSmall?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 14,
      ),
      bodyLarge: base.bodyLarge?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 20,
      ),
      bodyMedium: base.bodyMedium?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 18,
      ),
      bodySmall: base.bodySmall?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 16,
      ),
      labelLarge: base.labelLarge?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 20,
      ),
      labelMedium: base.labelMedium?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 18,
      ),
      labelSmall: base.labelSmall?.copyWith(
        fontFamily: "PlusJakartaSans",
        fontSize: 16,
      ),
    );

AppBarTheme _appBarTheme(AppBarTheme base) => base.copyWith(
      backgroundColor: AppColors.primary,
    );
