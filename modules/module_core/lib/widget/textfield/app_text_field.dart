import 'package:flutter/material.dart';
import 'package:module_core/widget/enum/text_field_type.dart';
import 'package:module_core/widget/theme/app_colors.dart';
import 'package:module_core/widget/textfield/text_field.dart';
import 'package:module_core/widget/theme/app_text_style.dart';

class AppTextField extends StatefulWidget {
  AppTextField({
    super.key,
    this.width,
    this.height = 60,
    this.type = TextFieldType.rectangle,
    this.borderwidth = 1,
    this.cornerradius = 0,
    required this.hintText,
    this.iconPrefix,
    this.iconSufix,
    this.backgroundColor,
    this.editingBorderColor = AppColors.primary,
    this.idleBorderColor = Colors.grey,
    this.normalBorderColor = Colors.grey,
    this.focusedBorderColor = AppColors.primary,
    this.errorBorderColor = AppColors.error,
    this.disabledBorderColor = Colors.black,
    this.paddingVertical = 3,
    this.paddingHorizontal = 0,
    this.marginVertical = 3,
    this.marginHorizontal = 0,
    this.controller,
    this.keyboardType = TextInputType.text,
    this.textCapitalization = TextCapitalization.none,
    this.style,
    this.textDirection = TextDirection.ltr,
    this.textAlign = TextAlign.start,
    this.textAlignVertical = TextAlignVertical.center,
    this.autofocus = false,
    this.readOnly = false,
    this.obscureText = false,
    this.maxLines = 1,
    this.minLines = 1,
    this.expands = true,
    this.maxLength,
    this.onChanged,
    this.onTap,
    this.onEditingComplete,
    this.onFieldSubmitted,
    this.onSaved,
    this.validator,
    this.enabled = true,
    this.color,
    this.borderRadius,
    this.scrollPhysics,
  });

  final double? width;
  final double? height;
  final TextFieldType? type;
  final Color normalBorderColor;
  final Color editingBorderColor;
  final Color focusedBorderColor;
  final Color errorBorderColor;
  final Color idleBorderColor;
  final Color disabledBorderColor;
  final Color? backgroundColor;
  final double borderwidth;
  final double cornerradius;
  final double paddingVertical;
  final double paddingHorizontal;
  final double marginVertical;
  final double marginHorizontal;
  final Widget? iconPrefix;
  final Widget? iconSufix;
  final TextEditingController? controller;
  final String hintText;
  final TextInputType? keyboardType;
  final TextCapitalization textCapitalization;
  final TextStyle? style;
  final TextDirection? textDirection;
  final TextAlign? textAlign;
  final TextAlignVertical? textAlignVertical;
  final bool autofocus;
  final bool? readOnly;

  bool obscureText;
  final int? maxLines;
  final int? minLines;
  final bool expands;
  final int? maxLength;
  final ValueChanged<String>? onChanged;
  final GestureTapCallback? onTap;
  final VoidCallback? onEditingComplete;
  final ValueChanged<String>? onFieldSubmitted;
  final FormFieldSetter<String>? onSaved;
  final FormFieldValidator<String>? validator;
  final bool enabled;
  final Color? color;
  final Radius? borderRadius;
  final ScrollPhysics? scrollPhysics;

  @override
  _AppTextFieldState createState() => _AppTextFieldState();
}

class _AppTextFieldState extends State<AppTextField> {
  late bool isShowPassword;

  @override
  void initState() {
    super.initState();
    setState(() {
      isShowPassword = widget.obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
      width: widget.width,
      margin: EdgeInsets.symmetric(
          vertical: widget.marginVertical, horizontal: widget.marginHorizontal),
      padding: EdgeInsets.symmetric(
          vertical: widget.paddingVertical,
          horizontal: widget.paddingHorizontal),
      child: MyTextField(
        decoration: InputDecoration(
          isDense: true,
          filled: widget.backgroundColor != null ||
              widget.type == TextFieldType.filled ||
              false,
          prefixIcon: widget.iconPrefix,
          suffixIcon: widget.obscureText
              ? IconButton(
                  onPressed: () => {
                        setState(() {
                          isShowPassword = !isShowPassword;
                        })
                      },
                  icon: isShowPassword
                      ? Icon(Icons.remove_red_eye)
                      : Icon(Icons.panorama_fish_eye))
              : widget.iconSufix,
          fillColor: widget.type == TextFieldType.filled
              ? AppColors.form
              : widget.backgroundColor,
          hintText: widget.hintText,
          border: OutlineInputBorder(
            borderSide: BorderSide(
              color: widget.type == TextFieldType.filled
                  ? AppColors.form
                  : widget.normalBorderColor,
              width:
                  widget.type == TextFieldType.filled ? 0 : widget.borderwidth,
            ),
            borderRadius: BorderRadius.circular(
                widget.type == TextFieldType.rounded ? 8 : widget.cornerradius),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: widget.editingBorderColor,
              width:
                  widget.type == TextFieldType.filled ? 0 : widget.borderwidth,
            ),
            borderRadius: BorderRadius.circular(
                widget.type == TextFieldType.rounded ? 8 : widget.cornerradius),
          ),
          disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: widget.type == TextFieldType.filled
                  ? AppColors.form
                  : widget.disabledBorderColor,
              width:
                  widget.type == TextFieldType.filled ? 0 : widget.borderwidth,
            ),
            borderRadius: BorderRadius.circular(
                widget.type == TextFieldType.rounded ? 8 : widget.cornerradius),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: widget.errorBorderColor,
              width:
                  widget.type == TextFieldType.filled ? 0 : widget.borderwidth,
            ),
            borderRadius: BorderRadius.circular(
                widget.type == TextFieldType.rounded ? 8 : widget.cornerradius),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: widget.type == TextFieldType.filled
                  ? AppColors.form
                  : widget.idleBorderColor,
              width:
                  widget.type == TextFieldType.filled ? 0 : widget.borderwidth,
            ),
            borderRadius: BorderRadius.circular(
                widget.type == TextFieldType.rounded ? 8 : widget.cornerradius),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: widget.errorBorderColor,
              width:
                  widget.type == TextFieldType.filled ? 0 : widget.borderwidth,
            ),
            borderRadius: BorderRadius.circular(
                widget.type == TextFieldType.rounded ? 8 : widget.cornerradius),
          ),
        ),
        controller: widget.controller,
        keyboardType: widget.keyboardType,
        textCapitalization: widget.textCapitalization,
        style: widget.style ?? AppTextStyles.displayLarge(context),
        textDirection: widget.textDirection,
        textAlign: widget.textAlign,
        textAlignVertical: widget.textAlignVertical,
        autofocus: widget.autofocus,
        readOnly: widget.readOnly,
        obscureText: isShowPassword,
        maxLines: widget.maxLines,
        minLines: widget.minLines,
        expands: widget.expands,
        maxLength: widget.maxLength,
        onChanged: widget.onChanged,
        onTap: widget.onTap,
        onEditingComplete: widget.onEditingComplete,
        onFieldSubmitted: widget.onFieldSubmitted,
        onfieldSaved: widget.onSaved,
        validator: widget.validator,
        fieldEnabled: widget.enabled,
        color: widget.color,
        borderRadius: widget.borderRadius,
        scrollPhysics: widget.scrollPhysics,
      ),
    );
  }
}
