import 'package:flutter/material.dart';
import 'package:module_core/widget/enum/avatar_type.dart';

class AppAvatar extends StatelessWidget {
  const AppAvatar(
      {super.key,
        this.width = 60,
        this.height = 60,
        this.backgroundColor,
        this.backgroundImage,
        this.foregroundColor,
        this.borderRadius,
        this.type = AvatarType.circle,});

  final Color? backgroundColor;
  final Color? foregroundColor;
  final ImageProvider? backgroundImage;
  final AvatarType type;
  final BorderRadius? borderRadius;
  final double? width;
  final double? height;

  BoxShape get _avatarShape {
    if (type == AvatarType.circle) {
      return BoxShape.circle;
    } else if (type == AvatarType.square) {
      return BoxShape.rectangle;
    } else if (type == AvatarType.rounded) {
      return BoxShape.rectangle;
    } else {
      return BoxShape.rectangle;
    }
  }

  @override
  Widget build(BuildContext context) {
    final Color? backgroundColor = this.backgroundColor;
    final Color? foregroundColor = this.foregroundColor;

    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: backgroundColor,
        image: backgroundImage != null
            ? DecorationImage(
          image: backgroundImage!,
          fit: BoxFit.cover,
        )
            : null,
        shape: _avatarShape,
        borderRadius: type == AvatarType.rounded && borderRadius == null
            ? BorderRadius.circular(10)
            : borderRadius,
      ),
    );
  }
}
