import 'package:flutter/material.dart';
import 'package:module_core/module_core.dart';
import 'package:module_core/widget/theme/app_text_style.dart';
import 'package:module_core/widget/utils/size_util.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          margin: const EdgeInsets.all(16),
          child: Column(
            children: [
              30.height,
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  AppText(
                    textAlign: TextAlign.center,
                    text: 'MD Tracking',
                    textStyle: AppTextStyles.headlineMedium(context)?.copyWith(
                      fontWeight: FontWeight.bold,
                      color: AppColors.primary,
                    ),
                  ),
                  8.height,
                  AppText(
                    textAlign: TextAlign.center,
                    text: 'Sign In',
                    textStyle: AppTextStyles.labelMedium(context)?.copyWith(
                      fontWeight: FontWeight.bold,
                      color: AppColors.primary,
                    ),
                  ),
                  8.height,
                ],
              ),
              Form(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AppText(
                      text: 'Username',
                      textStyle: AppTextStyles.displayLarge(context)?.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    AppTextField(
                      hintText: 'Username',
                      type: TextFieldType.rounded,
                    ),
                    4.height,
                    AppText(
                      text: 'Password',
                      textStyle: AppTextStyles.displayLarge(context)?.copyWith(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    AppTextField(
                      hintText: 'Password',
                      type: TextFieldType.rounded,
                      obscureText: true,
                      iconSufix: Icon(Icons.remove_red_eye),
                    ),
                    16.height,
                    AppButton(
                      onPressed: () => {
                        AppToast.showToast(
                          "Login",
                          context,
                          toastPosition: ToastPosition.bottom,
                          backgroundColor: AppColors.primary,
                          icon: Icon(Icons.add),
                          iconPosition: IconPosition.end,
                        ),
                      },
                      size: ButtonSize.xLarge,
                      fullWidthButton: true,
                      shape: ButtonShape.pills,
                      text: 'Login',
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
