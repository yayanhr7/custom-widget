enum AlertType {
  success,
  error,
  warning,
  info,
  custom,
}