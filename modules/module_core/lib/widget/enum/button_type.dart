enum ButtonType {
  solid,
  outline,
  outline2x,
  transparent,
}