class ButtonSize {
  static const double small = 30;
  static const double medium = 35;
  static const double large = 40;
  static const double xLarge = 45;
}