library module_dependencies;

export 'package:get_it/get_it.dart';
export 'package:injectable/injectable.dart';

export 'package:kt_dart/kt.dart';
export 'package:data_channel/data_channel.dart';
export 'package:dio/dio.dart';
export 'package:freezed_annotation/freezed_annotation.dart';
export 'package:shared_preferences/shared_preferences.dart';
export 'package:auto_route/auto_route.dart';
export 'package:connectivity/connectivity.dart';
