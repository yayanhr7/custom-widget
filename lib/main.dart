import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:module_core/module_core.dart';
import 'package:module_core/widget/appbar/app_bar.dart';
import 'package:module_core/widget/button/app_icon_button.dart';
import 'package:module_core/widget/menu/app_menu.dart';
import 'package:module_core/widget/theme/app_text_style.dart';
import 'package:module_core/widget/theme/app_theme.dart';
import 'package:module_core/widget/utils/size_util.dart';
import 'package:module_dependencies/module_dependencies.dart';

void main() {
  configureDependencies(kDebugMode ? Environment.dev : Environment.prod);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: appTheme,
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: MyAppBar(
        title: AppText(
          text: "testing",
          textStyle: AppTextStyles.labelMedium(context)
              ?.copyWith(color: AppColors.white),
        ),
        centerTitle: true,
        searchBar: true,
        onChanged: (e) => {print(e)},
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                AppText(
                  text: "Test",
                  type: TextType.displayLarge,
                ),
                Container(
                  child: AppMenu(
                    menuType: MenuType.grid,
                    items: [
                      MenuItem(
                        title: 'Title 1',
                        description: 'Description 1',
                        image: 'image_url_1',
                      ),
                      MenuItem(
                        title: 'Title 1',
                        description: 'Description 1',
                        image: 'image_url_1',
                      ),
                      MenuItem(
                        title: 'Title 2',
                        description: 'Description 2',
                        image: 'image_url_2',
                      ),
                      MenuItem(
                        title: 'Title 2',
                        description: 'Description 2',
                        image: 'image_url_2',
                      ),
                    ],
                  ),

                ),
                AppTextField(
                  hintText: 'sdf',
                  type: TextFieldType.rounded,
                ),
                AppTextField(
                  hintText: 'sdf',
                  type: TextFieldType.filled,
                  cornerradius: 8,
                  obscureText: true,
                  iconSufix: GestureDetector(
                    child: Icon(
                      Icons.remove_red_eye,
                    ),
                    onTap: () => {},
                  ),
                ),
                AppButton(
                    onPressed: () => {
                          AppToast.showToast(
                            "Toast",
                            context,
                            toastPosition: ToastPosition.bottom,
                            backgroundColor: AppColors.primary,
                            iconPosition: IconPosition.end,
                          ),
                        },
                    text: 'Toast'),
                AppButton(
                  onPressed: () => {
                    AppSnackbar.showSnackbar(
                      context,
                      'snackbar',
                      icon: Icon(Icons.add),
                    )
                  },
                  type: ButtonType.outline,
                  text: 'Snackbar',
                ),
                AppButton(
                  onPressed: () => {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()))
                  },
                  type: ButtonType.outline,
                  text: 'Login',
                ),
                AppButton(
                  text: 'Alert Dialog',
                  onPressed: () => {
                    AppAlertDialog.show(
                      context: context,
                      type: AlertType.success,
                      title: 'hehe',
                      text: "Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti font, tipografi, dan tata letak. Wikipedia",
                    )
                  },
                  fullWidthButton: true,
                ),
                AppCard(
                  listTile: AppListTile(
                    titleText: "Lorem ipsum",
                    subTitleText:
                        "Lorem ipsum, atau ringkasnya lipsum, adalah teks standar yang ditempatkan untuk mendemostrasikan elemen grafis atau presentasi visual seperti font, tipografi, dan tata letak. Wikipedia",
                  ),
                  image: Image.asset('assets/images/lorem_ipsum.png'),
                ),
                AppAvatar(
                  type: AvatarType.circle,
                  backgroundImage: AssetImage('assets/images/lorem_ipsum.png'),
                ),
                16.height,
                AppIconButton(
                  icon: Icon(Icons.add),
                  onPressed: () => {},
                ),
                16.height,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
